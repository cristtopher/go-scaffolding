# Go Project Scaffolding

![Banner](https://gitlab.com/cristtopher/go-scaffolding/downloads/gocristtopher.png)

This scaffolding should be used as the basis for projects in golang, it respects the principles of DDD and hexagonal architecture.

### Built With 🛠

- Go
- Hexagonal Architecture
- Domain Driven Design

## Diagram

![Hex]()

## Setup

### Requirements

- Go version 1.13.x
- Docker
- MongoDB

### Clone

```bash
git clone 
```

### Install dependencies

```bash
make install
```

### Check source code

```bash
make check
```

## Environment Variables

.env file

```
MONGO_URI=mongodb://<ip>:<port>/
MONGO_DBNAME=<dbname>
ENV=<dev/prod>
```

### Run locally

```bash
make run
```

### Build docker image

```bash
make docker-build
```

### Run on docker

```bash
make docker-run
```

### Structure

    -- go-scaffolding
        |-- builld: Strategy of construction of the device separated by environment.
          |-- docker
            |-- dev
              |-- Dockerfile: For local environment.
        |-- cmd: Starting point of the different layers of the device.
          |-- worker: Agregation's abstraction.
            |-- job.go: Main file for init dependencies & call job service.
        |-- pkg: Go source on package organization.
          |-- job: Aggregate containing use cases.
          |-- persistence: Data storage layer.
            |-- mongodb
        |-- .dockerignore
        |-- .gitignore
        |-- go.mod
        |-- go.sum
        |-- Makefile
        |-- README.md
        |-- CHANGELOG.md
