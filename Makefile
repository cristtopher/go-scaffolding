ifeq ($(OS),Windows_NT)
	IMAGE_NAME := $(shell basename "$(CD)")
	SSH_PRIVATE_KEY="$$(type ~/.ssh/id_rsa)"
else
	IMAGE_NAME := $(shell basename "$(PWD)")
	SSH_PRIVATE_KEY="$$(cat ~/.ssh/id_rsa)"
endif

install:
	go mod download

updatedeps:
	@echo Updating dependencies
	go get -d -v -u -f ./...

check:
	@echo Analyzing suspicious constructs
	go vet ./...

escape-analysis:
	@echo Analyzing the dynamic scope of pointers
	go build -gcflags='-m -l' -o bin/job cmd/worker/job.go

build:
	go build -o bin/job cmd/worker/job.go

compile:
	GO111MODULE=on \
	CGO_ENABLED=0 \
	GOOS=linux \
	GOARCH=amd64 \
	go build \
	-ldflags="-w -s" \
	-o ./job ./cmd/worker/job.go

test:
	go test -v ./...

run:
	go run cmd/worker/job.go

docker-build:
	docker build \
	-f build/docker/dev/Dockerfile \
	--build-arg SSH_PRIVATE_KEY=$(SSH_PRIVATE_KEY) \
	--build-arg O=$(OFFSET) \
	--build-arg L=$(LIMIT) \
	-t aqmarket/${IMAGE_NAME}:local .

docker-run:
	docker run --rm -it \
	--env-file ./.env \
	aqmarket/${IMAGE_NAME}:local -o=$(OFFSET) -l=$(LIMIT)	